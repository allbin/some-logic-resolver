let regexEscape = (s) => {
  return String(s).replace(/([-()[\]{}+?*.$^|,:#<!\\])/g, '\\$1').replace(/\x08/g, '\\x08');
};

let Operators = {
    boolean: {
        'is_unknown': {
            params: [],
            func: (name) => {
                return (stop) => {
                    return !stop.hasOwnProperty(name) || stop[name] === null;
                };
            }
        },
        'not_unknown': {
            params: [],
            func: (name) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && stop[name] !== null;
                };
            }
        },
        'exists': {
            params: [],
            func: (name) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && stop[name] === true;
                };
            }
        },
        'not_exists': {
            params: [],
            func: (name) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && stop[name] === false;
                };
            }
        }
    },
    number: {
        'is_unknown': {
            params: [],
            func: (name) => {
                return (stop) => {
                    return !stop.hasOwnProperty(name) || stop[name] === null;
                };
            }
        },
        'not_unknown': {
            params: [],
            func: (name) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && stop[name] !== null;
                };
            }
        },
        'eq': {
            params: ["number"],
            func: (name, val) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && stop[name] === val;
                };
            }
        },
        'lt': {
            params: ["number"],
            func: (name, val) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && stop[name] < val;
                };
            }
        },
        'lte': {
            params: ["number"],
            func: (name, val) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && stop[name] <= val;
                };
            }
        },
        'gt': {
            name: 'gt',
            params: ["number"],
            func: (name, val) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && stop[name] > val;
                };
            }
        },
        'gte': {
            name: 'gte',
            params: ["number"],
            func: (name, val) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && stop[name] >= val;
                };
            }
        },
        'not_between': {
            params: ["number", "number"],
            func: (name, min, max) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && (stop[name] < min || stop[name] > max);
                };
            }
        },
        'between': {
            params: ["number", "number"],
            func: (name, min, max) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && (stop[name] >= min && stop[name] <= max);
                };
            }
        },
        'is_one_of': {
            params: ["array:number"],
            func: (name, matches) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && matches.indexOf(stop[name]) !== -1;
                };
            }
        },
        'is_not_one_of': {
            params: ["array:number"],
            func: (name, matches) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && matches.indexOf(stop[name]) === -1;
                };
            }
        }
    },
    string: {
        'is_unknown': {
            params: [],
            func: (name) => {
                return (stop) => {
                    return !stop.hasOwnProperty(name) || stop[name] === null;
                };
            }
        },
        'not_unknown': {
            params: [],
            func: (name) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && stop[name] !== null;
                };
            }
        },
        'eq': {
            params: ["string"],
            func: (name, val) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && stop[name] === val;
                };
            }
        },
        'matches': {
            params: ["string"],
            func: (name, pattern) => {
                return (stop) => {
                    let re = new RegExp(regexEscape(pattern).replace(/\\\*/g, '.*'), "i");
                    return stop.hasOwnProperty(name) && re.test(stop[name]);
                };
            }
        },
        'not_matches': {
            params: ["string"],
            func: (name, pattern) => {
                return (stop) => {
                    let re = new RegExp(regexEscape(pattern).replace(/\\\*/g, '.*'), "i");
                    return stop.hasOwnProperty(name) && !re.test(stop[name]);
                };
            }
        },
        'is_one_of': {
            params: ["array:string"],
            func: (name, matches) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && matches.find(x => x.toLowerCase() === stop[name].toLowerCase());
                };
            }
        },
        'is_not_one_of': {
            params: ["array:string"],
            func: (name, matches) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && !matches.find(x => x.toLowerCase() === stop[name].toLowerCase());
                };
            }
        }
    },
    enum: {
        'is_unknown': {
            params: [],
            func: (name) => {
                return (stop) => {
                    return !stop.hasOwnProperty(name) || stop[name] === null;
                };
            }
        },
        'not_unknown': {
            params: [],
            func: (name) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && stop[name] !== null;
                };
            }
        },
        'eq': {
            params: ["string"],
            func: (name, val) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && stop[name] === val;
                };
            }
        },
        'is_one_of': {
            params: ["array:string"],
            func: (name, matches) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && matches.indexOf(stop[name]) !== -1;
                };
            }
        },
        'is_not_one_of': {
            params: ["array:string"],
            func: (name, matches) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && matches.indexOf(stop[name]) === -1;
                };
            }
        }
    },
    "array:string": {
        'is_unknown': {
            params: [],
            func: (name) => {
                return (stop) => {
                    return !stop.hasOwnProperty(name) || stop[name] === null;
                };
            }
        },
        'not_unknown': {
            params: [],
            func: (name) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && stop[name] !== null;
                };
            }
        },
        'contains_x_of': {
            params: ["array:string"],
            func: (name, matches) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && Array.isArray(stop[name]) && matches.filter(x => stop[name].find(y => y.toLowerCase() === x.toLowerCase())).length > 0;
                };
            }
        },
        'contains_none_of': {
            params: ["array:string"],
            func: (name, matches) => {
                return (stop) => {
                    // NOTE: will not return entries for which no property exists, even though they technically do not contain any of the matches
                    return stop.hasOwnProperty(name) && Array.isArray(stop[name]) && matches.filter(x => stop[name].find(y => y.toLowerCase() === x.toLowerCase())).length === 0;
                };
            }
        },
        'contains_all_of': {
            params: ["array:string"],
            func: (name, matches) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && Array.isArray(stop[name]) && matches.filter(x => stop[name].find(y => y.toLowerCase() === x.toLowerCase())).length === matches.length;
                };
            }
        }
    },
    date: {
        'is_unknown': {
            params: [],
            func: (name) => {
                return (stop) => {
                    return !stop.hasOwnProperty(name) || stop[name] === null;
                };
            }
        },
        'not_unknown': {
            params: [],
            func: (name) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && stop[name] !== null;
                };
            }
        },
        'before': {
            params: ["date"],
            func: (name, date) => {
                return (stop) => {
                    if (!stop.hasOwnProperty(name)) {
                        return false;
                    }
                    let val = stop[name];
                    if (typeof val === "string") {
                        val = new Date(val);
                    }
                    return ((val - date) < 0);
                };
            }
        },
        'after': {
            params: ["date"],
            func: (name, date) => {
                return (stop) => {
                    if (!stop.hasOwnProperty(name)) {
                        return false;
                    }
                    let val = stop[name];
                    if (typeof val === "string") {
                        val = new Date(val);
                    }

                    return ((val - date) >= 0);
                };
            }
        },
        'during': {
            params: ["date", "date"],
            func: (name, low, high) => {
                return (stop) => {
                    if (!stop.hasOwnProperty(name)) {
                        return false;
                    }
                    let val = stop[name];
                    if (typeof val === "string") {
                        val = new Date(val);
                    }
                    return ((val - low) >= 0) && ((val - high) < 0);
                };
            }
        }
    },
    "array:number": {
        'is_unknown': {
            params: [],
            func: (name) => {
                return (stop) => {
                    return !stop.hasOwnProperty(name) || stop[name] === null;
                };
            }
        },
        'not_unknown': {
            params: [],
            func: (name) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && stop[name] !== null;
                };
            }
        },
        'contains_one_or_more_of': {
            params: ["array:number"],
            func: (name, matches) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && Array.isArray(stop[name]) && matches.filter(x => stop[name].includes(x)).length > 0;
                };
            }
        },
        'contains_none_of': {
            params: ["array:number"],
            func: (name, matches) => {
                return (stop) => {
                    // NOTE: will not return entries for which no property exists, even though they technically do not contain any of the matches
                    return stop.hasOwnProperty(name) && Array.isArray(stop[name]) && matches.filter(x => stop[name].includes(x)).length === 0;
                };
            }
        },
        'contains_all_of': {
            params: ["array:number"],
            func: (name, matches) => {
                return (stop) => {
                    return stop.hasOwnProperty(name) && Array.isArray(stop[name]) && matches.filter(x => stop[name].includes(x)).length === matches.length;
                };
            }
        }
    },
    photo: {}
};

let OperatorTranslations = {
    "sv-SE": {
        is_unknown: "Saknar värde",
        not_unknown: "Har värde",
        exists: "Ja",
        not_exists: "Nej",
        eq: "Är lika med",
        lt: "Är mindre än",
        lte: "Är mindre än eller lika med",
        gt: "Är större än",
        gte: "Är större än eller lika med",
        between: "Är mellan eller lika med",
        not_between: "Är inte mellan eller lika med",
        is_one_of: "Är en av",
        is_not_one_of: "Är inte en av",
        matches: "Matchar",
        not_matches: "Matchar inte",
        contains_x_of: "Innehåller en eller flera av",
        contains_none_of: "Innehåller inga av",
        contains_all_of: "Innehåller alla av",
        before: "Är innan",
        after: "Är efter",
        during: "Är mellan"
    },
    "en-US": {
        is_unknown: "Has no value",
        not_unknown: "Has a value",
        exists: "Exists",
        not_exists: "Does not exist",
        eq: "Equals",
        lt: "Is less than",
        lte: "Is less than or equal to",
        gt: "Is greater than",
        gte: "Is greater than or equal to",
        between: "Is between or equal to",
        not_between: "Is not between or equal to",
        is_one_of: "Is one of",
        is_not_one_of: "Is not one of",
        matches: "Matches",
        not_matches: "Does not match",
        contains_x_of: "Contains one or more of",
        contains_none_of: "Contains none of",
        contains_all_of: "Contains all of",
        before: "Is before",
        after: "Is after",
        during: "Is between"
    }
};


class SomeLogicResolver {
    constructor(schema) {
        this.schema = this._processSchema(schema);
        this.filters = [];
    }

    _processSchema(schema) {
        let props = {};
        schema.properties.forEach((prop) => {
            props[prop.key] = prop;
        });
        return Object.assign({}, schema, {properties: props});
    }

    getAllSchemaKeys() {
        return Object.keys(this.schema.properties);
    }

    getAvailableOperatorsForKey(key) {
        let spec = this.schema.properties[key];
        if (!spec || !spec.type || !Operators.hasOwnProperty(spec.type)) {
            return {};
        }
        return Object.assign({}, Operators[spec.type]);
    }

    getTranslations() {
        return Object.assign({}, OperatorTranslations);
    }

    listFilters() {
        return this.filters.slice();
    }

    clearFilters() {
        this.filters = [];
    }

    dropFilter(filter) {
        let idx = this.filters.findIndex(x => x === filter);
        if (idx === -1) {
            return null;
        }
        return this.filters.splice(idx, 1);
    }

    validateFilterOpts(opts) {
        if (!opts.key) {
            throw new Error("Incomplete filter specification: missing key");
        }
        if (!opts.operator) {
            throw new Error("Incomplete filter specification: missing operator");
        }

        let spec = this.schema.properties[opts.key];
        if (!spec) {
            throw new Error("Invalid key: " + opts.key);
        }

        let operator = Operators[spec.type].hasOwnProperty(opts.operator) ? Operators[spec.type][opts.operator] : null;
        if (!operator) {
            throw new Error("Invalid operator: " + opts.operator);
        }

        let required_args = operator.hasOwnProperty("params") ? operator.params.length : 0;
        let supplied_args = opts.hasOwnProperty("args") ? opts.args.length : 0;
        if (required_args !== supplied_args) {
            throw new Error("Operator '" + opts.operator + "' requires exactly " + required_args + " argument(s), but " + supplied_args + " were supplied");
        }

        if (spec.type === "enum") {
            if (opts.args) {
                for (let i = 0 ; i < opts.args.length ; i++) {
                    let matches = opts.args[i];
                    for (let j = 0 ; j < matches.length ; j++) {
                        let match = matches[j];
                        if (spec.alternatives.indexOf(match) === -1) {
                            throw new Error("matches[" + i + "] is not a valid alternative for enum key '" + opts.key + "' (\"" + matches[i] + "\" vs " + JSON.stringify(spec.alternatives) + ")");
                        }
                    }
                }
            }
        }

        for (let i = 0 ; i < operator.params.length ; i++) {
            let p = operator.params[i];
            if (/^array:/.test(p)) {
                if (!Array.isArray(opts.args[i])) {
                    throw new Error("The '" + spec.type + ":" + opts.operator + "' operator requires args[" + i + "] to be an array");
                }
                let inner_type = p.split(':')[1];
                if (opts.args[i].length !== opts.args[i].filter(x => typeof x === inner_type).length) {
                    throw new Error("Some of the items in args[" + i + "] for '" + opts.key + "' are not of the correct type");
                }
            } else {
                if ((p === "date" && !(opts.args[i] instanceof Date) && (typeof opts.args[i] !== "string")) || (p !== "date" && typeof opts.args[i] !== p)) {
                    throw new Error("The '" + spec.type + ":" + opts.operator + "' operator requires args[" + i + "] to be of type " + p);
                }
            }
        }
    }

    createFilter(opts) {
        this.validateFilterOpts(opts);

        let spec = this.schema.properties[opts.key];
        let filter = Object.assign({}, { args: [] }, opts);

        filter.func = Operators[spec.type][filter.operator].func(filter.key, ...filter.args);

        return filter;
    }

    replaceFilter(old_filter, opts) {
        let new_filter = this.createFilter(opts);
        let idx = this.filters.findIndex(x => x === old_filter);
        if (idx === -1) {
            throw new Error("Tried to replace unknown filter");
        }

        this.filters.splice(idx, 1, new_filter);
        return new_filter;
    }

    addFilter(opts) {
        let filter = this.createFilter(opts);
        this.filters = this.filters.concat([filter]);
        return filter;
    }

    execute(stops) {
        let ret = stops.slice();
        for (let i = 0 ; i < this.filters.length ; i++) {
            ret = ret.filter(this.filters[i].func);
        }
        return ret;
    }
}

export default SomeLogicResolver;
