let pgp = require('pg-promise')();
import SomeLogicResolver from '../src/index.js';

function usage() {
    console.error("node test.js <filter-spec-json>");
    process.exit(1);
}

if (process.argv.length < 3) {
    usage();
}

let filter_specs = JSON.parse(process.argv[2]);

let db = pgp('postgresql://allbinary:allbinary@localhost/some');

Promise.resolve()
    .then(() => {
        return Promise.all([
            db.one("select data from schema"),
            db.any("select * from stops order by id")
        ]);
    })
    .then((res) => {
        let schema = res[0].data;
        let stops = res[1].map(x => x.data);

        let resolver = new SomeLogicResolver(schema);

        return [
            resolver,
            stops
        ];
    })
    .then((res) => {
        console.log((new Date()).toISOString(), "Got all stops");
        let resolver = res[0];
        let stops = res[1];

        filter_specs.forEach((fspec) => {
            resolver.addFilter(fspec);
        });

        return resolver.execute(stops);
    })
    .then((filtered_stops) => {
        console.log(filtered_stops.map((x) => { return {id: x["meta.id"], name: x["meta.name"]}; }));
        console.log((new Date()).toISOString(), filtered_stops.length + " stops matched the criteria");
        process.exit(0);
    })
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });
