'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var regexEscape = function regexEscape(s) {
    return String(s).replace(/([-()[\]{}+?*.$^|,:#<!\\])/g, '\\$1').replace(/\x08/g, '\\x08');
};

var Operators = {
    boolean: {
        'is_unknown': {
            params: [],
            func: function func(name) {
                return function (stop) {
                    return !stop.hasOwnProperty(name) || stop[name] === null;
                };
            }
        },
        'not_unknown': {
            params: [],
            func: function func(name) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && stop[name] !== null;
                };
            }
        },
        'exists': {
            params: [],
            func: function func(name) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && stop[name] === true;
                };
            }
        },
        'not_exists': {
            params: [],
            func: function func(name) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && stop[name] === false;
                };
            }
        }
    },
    number: {
        'is_unknown': {
            params: [],
            func: function func(name) {
                return function (stop) {
                    return !stop.hasOwnProperty(name) || stop[name] === null;
                };
            }
        },
        'not_unknown': {
            params: [],
            func: function func(name) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && stop[name] !== null;
                };
            }
        },
        'eq': {
            params: ["number"],
            func: function func(name, val) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && stop[name] === val;
                };
            }
        },
        'lt': {
            params: ["number"],
            func: function func(name, val) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && stop[name] < val;
                };
            }
        },
        'lte': {
            params: ["number"],
            func: function func(name, val) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && stop[name] <= val;
                };
            }
        },
        'gt': {
            name: 'gt',
            params: ["number"],
            func: function func(name, val) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && stop[name] > val;
                };
            }
        },
        'gte': {
            name: 'gte',
            params: ["number"],
            func: function func(name, val) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && stop[name] >= val;
                };
            }
        },
        'not_between': {
            params: ["number", "number"],
            func: function func(name, min, max) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && (stop[name] < min || stop[name] > max);
                };
            }
        },
        'between': {
            params: ["number", "number"],
            func: function func(name, min, max) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && stop[name] >= min && stop[name] <= max;
                };
            }
        },
        'is_one_of': {
            params: ["array:number"],
            func: function func(name, matches) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && matches.indexOf(stop[name]) !== -1;
                };
            }
        },
        'is_not_one_of': {
            params: ["array:number"],
            func: function func(name, matches) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && matches.indexOf(stop[name]) === -1;
                };
            }
        }
    },
    string: {
        'is_unknown': {
            params: [],
            func: function func(name) {
                return function (stop) {
                    return !stop.hasOwnProperty(name) || stop[name] === null;
                };
            }
        },
        'not_unknown': {
            params: [],
            func: function func(name) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && stop[name] !== null;
                };
            }
        },
        'eq': {
            params: ["string"],
            func: function func(name, val) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && stop[name] === val;
                };
            }
        },
        'matches': {
            params: ["string"],
            func: function func(name, pattern) {
                return function (stop) {
                    var re = new RegExp(regexEscape(pattern).replace(/\\\*/g, '.*'), "i");
                    return stop.hasOwnProperty(name) && re.test(stop[name]);
                };
            }
        },
        'not_matches': {
            params: ["string"],
            func: function func(name, pattern) {
                return function (stop) {
                    var re = new RegExp(regexEscape(pattern).replace(/\\\*/g, '.*'), "i");
                    return stop.hasOwnProperty(name) && !re.test(stop[name]);
                };
            }
        },
        'is_one_of': {
            params: ["array:string"],
            func: function func(name, matches) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && matches.find(function (x) {
                        return x.toLowerCase() === stop[name].toLowerCase();
                    });
                };
            }
        },
        'is_not_one_of': {
            params: ["array:string"],
            func: function func(name, matches) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && !matches.find(function (x) {
                        return x.toLowerCase() === stop[name].toLowerCase();
                    });
                };
            }
        }
    },
    enum: {
        'is_unknown': {
            params: [],
            func: function func(name) {
                return function (stop) {
                    return !stop.hasOwnProperty(name) || stop[name] === null;
                };
            }
        },
        'not_unknown': {
            params: [],
            func: function func(name) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && stop[name] !== null;
                };
            }
        },
        'eq': {
            params: ["string"],
            func: function func(name, val) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && stop[name] === val;
                };
            }
        },
        'is_one_of': {
            params: ["array:string"],
            func: function func(name, matches) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && matches.indexOf(stop[name]) !== -1;
                };
            }
        },
        'is_not_one_of': {
            params: ["array:string"],
            func: function func(name, matches) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && matches.indexOf(stop[name]) === -1;
                };
            }
        }
    },
    "array:string": {
        'is_unknown': {
            params: [],
            func: function func(name) {
                return function (stop) {
                    return !stop.hasOwnProperty(name) || stop[name] === null;
                };
            }
        },
        'not_unknown': {
            params: [],
            func: function func(name) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && stop[name] !== null;
                };
            }
        },
        'contains_x_of': {
            params: ["array:string"],
            func: function func(name, matches) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && Array.isArray(stop[name]) && matches.filter(function (x) {
                        return stop[name].find(function (y) {
                            return y.toLowerCase() === x.toLowerCase();
                        });
                    }).length > 0;
                };
            }
        },
        'contains_none_of': {
            params: ["array:string"],
            func: function func(name, matches) {
                return function (stop) {
                    // NOTE: will not return entries for which no property exists, even though they technically do not contain any of the matches
                    return stop.hasOwnProperty(name) && Array.isArray(stop[name]) && matches.filter(function (x) {
                        return stop[name].find(function (y) {
                            return y.toLowerCase() === x.toLowerCase();
                        });
                    }).length === 0;
                };
            }
        },
        'contains_all_of': {
            params: ["array:string"],
            func: function func(name, matches) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && Array.isArray(stop[name]) && matches.filter(function (x) {
                        return stop[name].find(function (y) {
                            return y.toLowerCase() === x.toLowerCase();
                        });
                    }).length === matches.length;
                };
            }
        }
    },
    date: {
        'is_unknown': {
            params: [],
            func: function func(name) {
                return function (stop) {
                    return !stop.hasOwnProperty(name) || stop[name] === null;
                };
            }
        },
        'not_unknown': {
            params: [],
            func: function func(name) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && stop[name] !== null;
                };
            }
        },
        'before': {
            params: ["date"],
            func: function func(name, date) {
                return function (stop) {
                    if (!stop.hasOwnProperty(name)) {
                        return false;
                    }
                    var val = stop[name];
                    if (typeof val === "string") {
                        val = new Date(val);
                    }
                    return val - date < 0;
                };
            }
        },
        'after': {
            params: ["date"],
            func: function func(name, date) {
                return function (stop) {
                    if (!stop.hasOwnProperty(name)) {
                        return false;
                    }
                    var val = stop[name];
                    if (typeof val === "string") {
                        val = new Date(val);
                    }

                    return val - date >= 0;
                };
            }
        },
        'during': {
            params: ["date", "date"],
            func: function func(name, low, high) {
                return function (stop) {
                    if (!stop.hasOwnProperty(name)) {
                        return false;
                    }
                    var val = stop[name];
                    if (typeof val === "string") {
                        val = new Date(val);
                    }
                    return val - low >= 0 && val - high < 0;
                };
            }
        }
    },
    "array:number": {
        'is_unknown': {
            params: [],
            func: function func(name) {
                return function (stop) {
                    return !stop.hasOwnProperty(name) || stop[name] === null;
                };
            }
        },
        'not_unknown': {
            params: [],
            func: function func(name) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && stop[name] !== null;
                };
            }
        },
        'contains_one_or_more_of': {
            params: ["array:number"],
            func: function func(name, matches) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && Array.isArray(stop[name]) && matches.filter(function (x) {
                        return stop[name].includes(x);
                    }).length > 0;
                };
            }
        },
        'contains_none_of': {
            params: ["array:number"],
            func: function func(name, matches) {
                return function (stop) {
                    // NOTE: will not return entries for which no property exists, even though they technically do not contain any of the matches
                    return stop.hasOwnProperty(name) && Array.isArray(stop[name]) && matches.filter(function (x) {
                        return stop[name].includes(x);
                    }).length === 0;
                };
            }
        },
        'contains_all_of': {
            params: ["array:number"],
            func: function func(name, matches) {
                return function (stop) {
                    return stop.hasOwnProperty(name) && Array.isArray(stop[name]) && matches.filter(function (x) {
                        return stop[name].includes(x);
                    }).length === matches.length;
                };
            }
        }
    },
    photo: {}
};

var OperatorTranslations = {
    "sv-SE": {
        is_unknown: "Saknar värde",
        not_unknown: "Har värde",
        exists: "Ja",
        not_exists: "Nej",
        eq: "Är lika med",
        lt: "Är mindre än",
        lte: "Är mindre än eller lika med",
        gt: "Är större än",
        gte: "Är större än eller lika med",
        between: "Är mellan eller lika med",
        not_between: "Är inte mellan eller lika med",
        is_one_of: "Är en av",
        is_not_one_of: "Är inte en av",
        matches: "Matchar",
        not_matches: "Matchar inte",
        contains_x_of: "Innehåller en eller flera av",
        contains_none_of: "Innehåller inga av",
        contains_all_of: "Innehåller alla av",
        before: "Är innan",
        after: "Är efter",
        during: "Är mellan"
    },
    "en-US": {
        is_unknown: "Has no value",
        not_unknown: "Has a value",
        exists: "Exists",
        not_exists: "Does not exist",
        eq: "Equals",
        lt: "Is less than",
        lte: "Is less than or equal to",
        gt: "Is greater than",
        gte: "Is greater than or equal to",
        between: "Is between or equal to",
        not_between: "Is not between or equal to",
        is_one_of: "Is one of",
        is_not_one_of: "Is not one of",
        matches: "Matches",
        not_matches: "Does not match",
        contains_x_of: "Contains one or more of",
        contains_none_of: "Contains none of",
        contains_all_of: "Contains all of",
        before: "Is before",
        after: "Is after",
        during: "Is between"
    }
};

var SomeLogicResolver = function () {
    function SomeLogicResolver(schema) {
        _classCallCheck(this, SomeLogicResolver);

        this.schema = this._processSchema(schema);
        this.filters = [];
    }

    _createClass(SomeLogicResolver, [{
        key: '_processSchema',
        value: function _processSchema(schema) {
            var props = {};
            schema.properties.forEach(function (prop) {
                props[prop.key] = prop;
            });
            return Object.assign({}, schema, { properties: props });
        }
    }, {
        key: 'getAllSchemaKeys',
        value: function getAllSchemaKeys() {
            return Object.keys(this.schema.properties);
        }
    }, {
        key: 'getAvailableOperatorsForKey',
        value: function getAvailableOperatorsForKey(key) {
            var spec = this.schema.properties[key];
            if (!spec || !spec.type || !Operators.hasOwnProperty(spec.type)) {
                return {};
            }
            return Object.assign({}, Operators[spec.type]);
        }
    }, {
        key: 'getTranslations',
        value: function getTranslations() {
            return Object.assign({}, OperatorTranslations);
        }
    }, {
        key: 'listFilters',
        value: function listFilters() {
            return this.filters.slice();
        }
    }, {
        key: 'clearFilters',
        value: function clearFilters() {
            this.filters = [];
        }
    }, {
        key: 'dropFilter',
        value: function dropFilter(filter) {
            var idx = this.filters.findIndex(function (x) {
                return x === filter;
            });
            if (idx === -1) {
                return null;
            }
            return this.filters.splice(idx, 1);
        }
    }, {
        key: 'validateFilterOpts',
        value: function validateFilterOpts(opts) {
            if (!opts.key) {
                throw new Error("Incomplete filter specification: missing key");
            }
            if (!opts.operator) {
                throw new Error("Incomplete filter specification: missing operator");
            }

            var spec = this.schema.properties[opts.key];
            if (!spec) {
                throw new Error("Invalid key: " + opts.key);
            }

            var operator = Operators[spec.type].hasOwnProperty(opts.operator) ? Operators[spec.type][opts.operator] : null;
            if (!operator) {
                throw new Error("Invalid operator: " + opts.operator);
            }

            var required_args = operator.hasOwnProperty("params") ? operator.params.length : 0;
            var supplied_args = opts.hasOwnProperty("args") ? opts.args.length : 0;
            if (required_args !== supplied_args) {
                throw new Error("Operator '" + opts.operator + "' requires exactly " + required_args + " argument(s), but " + supplied_args + " were supplied");
            }

            if (spec.type === "enum") {
                if (opts.args) {
                    for (var i = 0; i < opts.args.length; i++) {
                        var matches = opts.args[i];
                        for (var j = 0; j < matches.length; j++) {
                            var match = matches[j];
                            if (spec.alternatives.indexOf(match) === -1) {
                                throw new Error("matches[" + i + "] is not a valid alternative for enum key '" + opts.key + "' (\"" + matches[i] + "\" vs " + JSON.stringify(spec.alternatives) + ")");
                            }
                        }
                    }
                }
            }

            for (var _i = 0; _i < operator.params.length; _i++) {
                var p = operator.params[_i];
                if (/^array:/.test(p)) {
                    (function () {
                        if (!Array.isArray(opts.args[_i])) {
                            throw new Error("The '" + spec.type + ":" + opts.operator + "' operator requires args[" + _i + "] to be an array");
                        }
                        var inner_type = p.split(':')[1];
                        if (opts.args[_i].length !== opts.args[_i].filter(function (x) {
                            return (typeof x === 'undefined' ? 'undefined' : _typeof(x)) === inner_type;
                        }).length) {
                            throw new Error("Some of the items in args[" + _i + "] for '" + opts.key + "' are not of the correct type");
                        }
                    })();
                } else {
                    if (p === "date" && !(opts.args[_i] instanceof Date) && typeof opts.args[_i] !== "string" || p !== "date" && _typeof(opts.args[_i]) !== p) {
                        throw new Error("The '" + spec.type + ":" + opts.operator + "' operator requires args[" + _i + "] to be of type " + p);
                    }
                }
            }
        }
    }, {
        key: 'createFilter',
        value: function createFilter(opts) {
            var _Operators$spec$type$;

            this.validateFilterOpts(opts);

            var spec = this.schema.properties[opts.key];
            var filter = Object.assign({}, { args: [] }, opts);

            filter.func = (_Operators$spec$type$ = Operators[spec.type][filter.operator]).func.apply(_Operators$spec$type$, [filter.key].concat(_toConsumableArray(filter.args)));

            return filter;
        }
    }, {
        key: 'replaceFilter',
        value: function replaceFilter(old_filter, opts) {
            var new_filter = this.createFilter(opts);
            var idx = this.filters.findIndex(function (x) {
                return x === old_filter;
            });
            if (idx === -1) {
                throw new Error("Tried to replace unknown filter");
            }

            this.filters.splice(idx, 1, new_filter);
            return new_filter;
        }
    }, {
        key: 'addFilter',
        value: function addFilter(opts) {
            var filter = this.createFilter(opts);
            this.filters = this.filters.concat([filter]);
            return filter;
        }
    }, {
        key: 'execute',
        value: function execute(stops) {
            var ret = stops.slice();
            for (var i = 0; i < this.filters.length; i++) {
                ret = ret.filter(this.filters[i].func);
            }
            return ret;
        }
    }]);

    return SomeLogicResolver;
}();

exports.default = SomeLogicResolver;